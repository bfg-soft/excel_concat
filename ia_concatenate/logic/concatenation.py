import pandas as pd
from .base import Base

__all__ = [
    'DataFrameConcatenator',
]


class DataFrameConcatenator(Base):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._output = pd.DataFrame()

    def concatenate(self, input_files_list):
        logger = self._logger

        for input_file in input_files_list:
            logger.info(
                'Начинаем считывание файла {}.'.format(input_file)
            )
            try:
                logger.debug('Пробуем считать как csv.')
                df = pd.read_csv(
                    input_file,
                    header=0,
                    delimiter=';',
                    skip_blank_lines=True,
                    skipinitialspace=True,
                    encoding='utf-8'
                )
                logger.info('Успешно считано как csv.')
            except:
                logger.debug('Пробуем считать как xlsx.')
                df = pd.read_excel(input_file, header=0)
                logger.info('Успешно считано как xlsx.')

            self._output = pd.concat([self._output, df], sort=False)

    def write_to_xlsx(self, output_file, worksheet='Sheet1'):
        logger = self._logger

        logger.debug(
            'Начинаем запись в таблицу Excel {}.'.format(output_file)
        )

        with pd.ExcelWriter(output_file, engine='xlsxwriter') as writer:
            self._output.to_excel(
                writer,
                sheet_name=worksheet,
                index=False,
                freeze_panes=(1, 0)
            )  # send df to writer
            worksheet = writer.sheets[worksheet]  # pull worksheet object
            for idx, col in enumerate(self._output):
                # loop through all columns
                series = self._output[col]
                max_len = max((
                    series.astype(str).map(len).max(),  # len of largest item
                    len(str(series.name))  # len of column name/header
                )) + 1  # adding a little extra space
                worksheet.set_column(idx, idx, max_len)  # set column width
            writer.save()

        logger.info(
            'Запись в таблицу Excel {} успешно завершена.'.format(output_file)
        )
