from argparse import ArgumentParser
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

from ia_concatenate.config import read_config
from ia_concatenate.logic import DataFrameConcatenator

__all__ = [
    'run_concatenation',
]


def run_concatenation():
    parser = ArgumentParser(
        description='Консольный инструмент конкатенации нескольких таблиц.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'concatenate.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(level=args.debug and DEBUG or INFO)

    config = read_config(args.config)

    concat = DataFrameConcatenator()

    concat.concatenate(config['input'])
    concat.write_to_xlsx(config['output'])


if __name__ == '__main__':
    run_concatenation()
