from setuptools import find_packages, setup

setup(
    name='ia_concatenate',
    version='0.1.0a1',
    packages=find_packages(),
    url='',
    license='MIT',
    author='BFG-Soft',
    author_email='saa@bfg-soft.ru',
    description='Tool to concatenate several tables',
    install_requires=[
        'PyYAML',
        'pandas',
        'xlrd',
        'xlsxwriter'
    ],
    entry_points={
        'console_scripts': [
            'ia_concatenation = ia_concatenate.scripts:run_concatenation'
        ]
    }
)
